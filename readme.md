**Grab contacts from email accounts with PHP**
===================

This readme is for grab contacts from Google, Yahoo and Hotmail accounts using his API's.


**Setup SSL **
-------------

For several API's they require that's you have a domain with the security layer enabled for making request to them. This how to is gonna cover how to setup in **Ubuntu 14** with **LAMP**.

###Setup Apache

##### **Enable mod SSL to Apache**
> sudo a2enmod ssl
##### **Restart Apache**
> sudo service apache2 restart
##### **Create ssl directory**
> sudo mkdir /etc/apache2/ssl
##### **Place the new certificate**
> sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/apache2/ssl/apache.key -out /etc/apache2/ssl/apache.crt

*e.g of params:*
>Country Name (2 letter code) [AU]:US
 State or Province Name (full name) [Some-State]:New York
			Locality Name (eg, city) []:New York City
			Organization Name (eg, company) [Internet Widgits Pty Ltd]:Your Company
			Organizational Unit Name (eg, section) []:Department of Kittens
			Common Name (e.g. server FQDN or YOUR name) []:your_domain.com
			Email Address []:your_email@domain.com
			
##### **Create a new virtualhost**
You must create the directory **ssltest** in your html folder
> sudo mkdir /var/www/html/ssltest

Add the virtualhost pointing that folder
> sudo vim /etc/apache2/sites-available/default-ssl.conf
```
<IfModule mod_ssl.c>
    <VirtualHost _default_:443>
        ServerAdmin admin@ssltest.com
        ServerName ssltest.com
        ServerAlias www.ssltest.com
        DocumentRoot /var/www/html/ssltest
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
        SSLEngine on
        SSLCertificateFile /etc/apache2/ssl/apache.crt
        SSLCertificateKeyFile /etc/apache2/ssl/apache.key
        <FilesMatch "\.(cgi|shtml|phtml|php)$">
                        SSLOptions +StdEnvVars
        </FilesMatch>
        <Directory /usr/lib/cgi-bin>
                        SSLOptions +StdEnvVars
        </Directory>
        BrowserMatch "MSIE [2-6]" \
                        nokeepalive ssl-unclean-shutdown \
                        downgrade-1.0 force-response-1.0
        BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
    </VirtualHost>
</IfModule>
```
##### **Activate the virtualhost**
> sudo a2ensite default-ssl.conf
##### **Restart Apache**
> sudo service apache2 restart

###Setup DNS
You need to add your local ip to the **/etc/hosts** file

> 127.0.0.1      www.ssltest.com

Google GMail
-------------
Go to the developer center https://console.developers.google.com and create a new application called **retrieve-contacts**, with this configuration:

> Redirect url: http://www.ssltest.com/google-api-php-client/examples/contacts.php

Also you need to keep the Client ID and Client Secret generated from Google.

>**Contacts Rest API documentation https://developers.google.com/google-apps/contacts/v3/reference**

###Creating the example

####Get the PHP library
For google we need to clone the git project  https://github.com/google/google-api-php-client inside the **ssltest folder**.




> git clone https://github.com/google/google-api-php-client.git .

and install with composer.

> composer install



####Create the example


Now you need to create the file **contacts.php** inside **ssltest/google-api-php-client/examples/** folder.
```
<?php

include_once "templates/base.php";
session_start();

require_once realpath(dirname(__FILE__) . '/../src/Google/autoload.php');

$client_id = '{CLIENT_ID}';
$client_secret = '{CLIENT_SECRET}';
$redirect_uri = 'https://www.ssltest.com/google/google-api-php-client/examples/contacts.php';

$client = new Google_Client();
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
//$client->addScope("https://www.google.com/m8/feeds/contacts/default/full");
$client->addScope("https://www.google.com/m8/feeds/");

if (isset($_REQUEST['logout']))
{
    unset($_SESSION['access_token']);
}

if (isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $_SESSION['access_token'] = $client->getAccessToken();
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['access_token']) && $_SESSION['access_token'])
{
    $client->setAccessToken($_SESSION['access_token']);
}
else
{
    $authUrl = $client->createAuthUrl();
}

if ($client->getAccessToken())
{
    $req  = new Google_Http_Request("https://www.google.com/m8/feeds/contacts/default/full?max-results=1000");
    $val = $client->getAuth()->authenticatedRequest($req);
    $xml=  new SimpleXMLElement($val->getResponseBody());
    $xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');

    $result = $xml->xpath('//gd:email');

    $count = 0;
    foreach ($result as $title)
    {
      $count++;
      echo $count.". ".$title->attributes()->address . "<br>";

    }
}
?>
<div class="box">
  <div class="request">
<?php 
if (isset($authUrl)) {
  echo "<a class='login' href='" . $authUrl . "'>Connect Me!</a>";
} else {
  echo <<<END
    <a class='logout' href='?logout'>Logout</a>
END;
}
?>
  </div>
</div>
```

####Try it!

Just go to https://www.ssltest.com/google-api-php-client/examples/contacts.php and test the login and retrieve all the contact list : ) .

Yahoo Mail
-------------
Go to the developer center https://developer.apps.yahoo.com/projects and create a new application called **retrieve-contacts**.

**Note:** You must activate the **Contacts API** permission

Also you need to keep the Consumer Key and Consumer Secret generated from Yahoo.

###Creating the example

####Get the PHP library
For Yahoo we need to clone the git project  https://github.com/yahoo/yos-social-php and copy de **lib** folder inside **ssltest/yahoo** folder.

####Create the example

Now you need to create the file **contacts.php** inside **ssltest/yahoo/** folder.
```
<?php
// Include the YOS library.
require dirname(__FILE__).'/lib/Yahoo.inc';

//for converting xml to array
function XmltoArray($xml)
{
    $array = json_decode(json_encode($xml), TRUE);
    foreach (array_slice($array, 0) as $key => $value )
    {
        if (empty($value) ){
        	$array[$key] = NULL;
        }
        elseif (is_array($value) ){
			$array[$key] = XmltoArray($value);
		}
    }

    return $array;
}

// debug settings
//error_reporting(E_ALL | E_NOTICE); # do not show notices as library is php4 compatable
//ini_set('display_errors', true);
YahooLogger::setDebug(true);
YahooLogger::setDebugDestination('LOG');

// use memcache to store oauth credentials via php native sessions
//ini_set('session.save_handler', 'files');
//session_save_path('/tmp/');
session_start();

// Make sure you obtain application keys before continuing by visiting:
// https://developer.yahoo.com/dashboard/createKey.html

define('OAUTH_CONSUMER_KEY', '{CONSUMER_KEY}');
define('OAUTH_CONSUMER_SECRET', '{CONSUMER_SECRET}');
define('OAUTH_DOMAIN', 'ssltest.com');
define('OAUTH_APP_ID', '{APP_ID}');

if (array_key_exists("logout", $_GET))
{
    // if a session exists and the logout flag is detected
    // clear the session tokens and reload the page.
    YahooSession::clearSession();
    header("Location: index.php");
}

// check for the existance of a session.
// this will determine if we need to show a pop-up and fetch the auth url,
// or fetch the user's social data.
$hasSession = YahooSession::hasSession(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_APP_ID);

if ($hasSession === false)
{
    // create the callback url,
    $callback = YahooUtil::current_url();
    $sessionStore = new NativeSessionStore();
    // pass the credentials to get an auth url.
    // this URL will be used for the pop-up.
    $auth_url = YahooSession::createAuthorizationUrl(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, $callback, $sessionStore);
}
else
{
    // pass the credentials to initiate a session
    $session = YahooSession::requireSession(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_APP_ID);
    // if a session is initialized, fetch the user's profile information
    if ($session)
    {
        // Get the currently sessioned user.
        $user = $session->getSessionedUser();

        // Load the profile for the current user.
        $profile = $user->getProfile();
        $profile_contacts=XmltoArray($user->getContactSync());
        $contacts=array();
        foreach ($profile_contacts['contactsync']['contacts'] as $key=>$profileContact)
        {
           foreach ($profileContact['fields'] as $contact)
           {
              $contacts[$key][$contact['type']]=$contact['value'];
           }
        }
    }
}

function close_popup() {
?>
<script type="text/javascript">
  window.close();
</script>
<?php
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
  <head>
    <title>Get the contact list from Yahoo-Idiot Minds</title>
<style type="text/css">
body{
background-color: #F2F2F2;
}
.yh_frnds{
	list-style:none;
}
.yh_frnds li{
	padding:10px;
	float:left;
	width:30%;
}
.frnd_list{
	margin-top:-25px;
	margin-left:40px;
}
.fb_frnds a{
		text-decoration:none;
		 background: #333;
		 filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#333', endColorstr='#D95858'); /* for IE */
background: -webkit-gradient(linear, left top, left bottom, from(#333), to(#D95858)); /* for webkit browsers */
background: -moz-linear-gradient(top,  #333,  #D95858)/* for firefox 3.6+ */ ;
    color: #FFFFFF;
		float: right;
		font: bold 13px arial;
		margin-right:110px ;
}
</style>
  </head>
  <body>
    <?php
        if ($hasSession === false)
        {
    ?>
            <a href="<?php echo $auth_url; ?>">LOG IN</a>
<?php   }
        else if ($hasSession && $profile)
        {
?>
            <img src="<?php echo $profile->image->imageUrl; ?>" style="width:10%;" />
            <h2>Hi <a href="<?php echo $profile->profileUrl; ?>" target="_blank"><?php echo $profile->nickname; ?></a>
            </h2>

<?php
            if ($profile->status->message != "")
            {
                $statusDate = date('F j, y, g:i a', strtotime($profile->status->lastStatusModified));
                echo sprintf("<p><strong>&#8220;</strong>%s<strong>&#8221;</strong> on %s</p>", $profile->status->message, $statusDate);
            }

                echo "<p><a href=\"?logout\">Logout</a></p>"; ?>

            <ul  class="yh_frnds">
        <?php
            foreach ($contacts as $user_friend)
            {

                if(isset($user_friend['email']))
                {
        ?>
                    <li >
                        <img src="yahoo1.png" width="30" height="30"/>
                        <div  class="frnd_list"><strong><?php echo $user_friend['name']['givenName']; ?>
                            </strong><br /><?php echo $user_friend['email'];?>
                        </div>
                    </li>

<?php           }
            }
?>
</ul>
 <?php  } ?>
  </body>
</html>

```
####Try it!

Just go to https://www.ssltest.com/yahoo/contacts.php and test the login and retrieve all the contact list : ) .

Hotmail
-------------
Go to the developer center https://account.live.com/developers/applications/index and create a new application called **retrieve-contacts**, with this configuration:

> Redirect url: https://www.ssltest.com/hotmail/result.php

Also you need to keep the Client ID and Client Secret generated from Google.

>**This is the Rest API documentation https://msdn.microsoft.com/en-us/library/hh826543.aspx#rest**

###Creating the example

####config.php
```
<?php
$client_id = '{CLIENT_ID}';
$client_secret = '{CLIENT_SECRET}';
$redirect_uri = 'https://www.ssltest.com/hotmail/result.php';
?>
```
####index.php
```
<?php
	include('config.php');
	$url = 'https://login.live.com/oauth20_authorize.srf?client_id='.$client_id.'&scope=wl.signin%20wl.basic%20wl.emails%20wl.contacts_emails&response_type=code&redirect_uri='.$redirect_uri;
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Invite Friends</title>
</head>

<body link="#0C6182" vlink="#0C6182" alink="#0C6182">
<a href="<?php print $url; ?>" style="text-decoration: none">
Invite Friends From Hotmail</a>
<br>
</body>

</html>
```

####result.php
```
<?php 

session_start();

function curl_file_get_contents($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Invite Friends</title>
</head>

<body link="#0C6182" vlink="#0C6182" alink="#0C6182">
<div align="center">
	<table border="0" width="80%" cellspacing="0" cellpadding="0" style="border: 1px solid #0C6182; padding: 0">

		<tr>
			<td align="left" style="padding:20px; font-family: Trebuchet MS; font-size: 12pt; line-height:110%" valign="top">
<?php
//setting parameters

include('config.php');

$auth_code = $_GET["code"];
$fields=array(
	'code'=>  urlencode($auth_code),
	'client_id'=>  urlencode($client_id),
	'client_secret'=>  urlencode($client_secret),
	'redirect_uri'=>  urlencode($redirect_uri),
	'grant_type'=>  urlencode('authorization_code')
);

$post = '';
foreach($fields as $key=>$value)
{
	$post .= $key.'='.$value.'&';
}

$post = rtrim($post,'&');
$curl = curl_init();
curl_setopt($curl,CURLOPT_URL,'https://login.live.com/oauth20_token.srf');
curl_setopt($curl,CURLOPT_POST,5);
curl_setopt($curl,CURLOPT_POSTFIELDS,$post);
curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
$result = curl_exec($curl);
curl_close($curl);
$response =  json_decode($result);
$accesstoken = $response->access_token;

$url = 'https://apis.live.net/v5.0/me/contacts?access_token='.$accesstoken;
$xmlresponse =  curl_file_get_contents($url);
$xml = json_decode($xmlresponse, true);
$contacts_email = "";

$count = 0;
foreach ($xml['data'] as $title) {
	$count++;
	echo $count.". ".$title['emails']['personal'] . "<br><br>";
}

?>

&nbsp;</td>
		</tr>
	</table>
</div>
</body>
```
